# Firebase Friendly Chat Codelab

Este codalab es una versión resultante de [Firebase: Build a Real Time Web Chat App Codelab](https://codelabs.developers.google.com/codelabs/firebase-web/index.html)
y [Firebase: Build a Real Time Web Chat App with Functions Codelab](https://codelabs.developers.google.com/codelabs/firebase-cloud-functions/index.html)

En las diferentes 'branches' se encuentran los bloques de código necesarios para implementar las características y funciones que se agregan
para usar múltiples características de Firebase.

- [Paso 1](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/01-firebase-setup): Configuración Inicial
- [Paso 2](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/02-firebase-sign-in): Firebase Sign-In
- [Paso 3](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/03-firebase-save-messages): Firebase Realtime Database
- [Paso 4](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/04-firebase-store-images): Firebase Storage
- [Paso 5](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/05-firebase-cloud-messaging): Fiebase Cloud Messaging
- [Paso 6-a](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/06-firebase-security-rules-a): Database Security Rules 
- [Paso 6-b](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/06-firebase-security-rules-b): Storage Security Rules
- [Paso 7-a](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/07-firebase-functions-a): Mensajes con Firebase Functions
- [Paso 7-b](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/07-firebase-functions-b): Vision API con Firebase Functions
- [Paso 7-c](https://gitlab.com/tonioguzmanf/firebase-chat-functions/tree/07-firebase-functions-c): Notificaciones con Firebase Functions
