/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Inicializa FriendlyChat.
function FriendlyChat() {
  this.checkSetup();

  // Elementos DOM.
  this.messageList = document.getElementById('messages');
  this.messageForm = document.getElementById('message-form');
  this.messageInput = document.getElementById('message');
  this.submitButton = document.getElementById('submit');
  this.submitImageButton = document.getElementById('submitImage');
  this.imageForm = document.getElementById('image-form');
  this.mediaCapture = document.getElementById('mediaCapture');
  this.userPic = document.getElementById('user-pic');
  this.userName = document.getElementById('user-name');
  this.signInButton = document.getElementById('sign-in');
  this.signOutButton = document.getElementById('sign-out');
  this.signInSnackbar = document.getElementById('must-signin-snackbar');

  // Guarda mensajes on form submit.
  this.messageForm.addEventListener('submit', this.saveMessage.bind(this));
  this.signOutButton.addEventListener('click', this.signOut.bind(this));
  this.signInButton.addEventListener('click', this.signIn.bind(this));

  // Toggle para el button.
  var buttonTogglingHandler = this.toggleButton.bind(this);
  this.messageInput.addEventListener('keyup', buttonTogglingHandler);
  this.messageInput.addEventListener('change', buttonTogglingHandler);

  // Eventos pra subida de imágenes.
  this.submitImageButton.addEventListener('click', function(e) {
    e.preventDefault();
    this.mediaCapture.click();
  }.bind(this));
  this.mediaCapture.addEventListener('change', this.saveImageMessage.bind(this));

  this.initFirebase();
}

// Características Firebase e inicialización de firebase auth.
FriendlyChat.prototype.initFirebase = function() {
  // TODO(DEVELOPER): Inicializa Firebase.
};

// Carga historial de mensajes del chat y escucha por los siguientes.
FriendlyChat.prototype.loadMessages = function() {
  // TODO(DEVELOPER): Carga y escucha nuevos mensajes.
};

// Guarda un nuevo mensaje en la Firebase DB.
FriendlyChat.prototype.saveMessage = function(e) {
  e.preventDefault();
  // Verifica que el usuario que ingresó un mensaje ha iniciado sesión.
  if (this.messageInput.value && this.checkSignedInWithMessage()) {

    // TODO(DEVELOPER): enviar un nuevo mensaje a Firebase.

  }
};

// Asigna la URL de la imagen en Cloud Storage al elemento img.
FriendlyChat.prototype.setImageUrl = function(imageUri, imgElement) {
  imgElement.src = imageUri;

  // TODO(DEVELOPER): Recupera la URL de la imagen en Cloud Storage y lo asigna al src del elemento img.
};

// Guarda en Firebase un mensaje nuevo que contiene la URI de la imagen.
// La imagen es gudarda en Firebase storage.
FriendlyChat.prototype.saveImageMessage = function(event) {
  event.preventDefault();
  var file = event.target.files[0];

  // Borra la selección en el file picker.
  this.imageForm.reset();

  // Verifica si el archivo es una imagen.
  if (!file.type.match('image.*')) {
    var data = {
      message: 'Solo puedes compartir imágenes',
      timeout: 2000
    };
    this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
    return;
  }
  // Verifica si el usuario ha accedido
  if (this.checkSignedInWithMessage()) {

    // TODO(DEVELOPER): Sube una imagen a Firebase storage y agrega el mensaje.

  }
};

// Acceso al Friendly Chat.
FriendlyChat.prototype.signIn = function() {
  // TODO(DEVELOPER): Acceder en Firebase con credenciales de Google.
};

// Salir del Friendly Chat.
FriendlyChat.prototype.signOut = function() {
  // TODO(DEVELOPER): Salir de Firebase.
};

// Triggers para los eventos signs-in y signs-out.
FriendlyChat.prototype.onAuthStateChanged = function(user) {
  if (user) { // El usuario ha accedido
    // Obtener la foto de perfil y el nombre del usuario del objeto user de Firebase.
    var profilePicUrl = null;   // TODO(DEVELOPER): Obtener la foto de perfil.
    var userName = null;        // TODO(DEVELOPER): Obtener el nombre del usuario.

    // Asignar la foto de perfil y el nombre.
    this.userPic.style.backgroundImage = 'url(' + profilePicUrl + ')';
    this.userName.textContent = userName;

    // Mostrar el perfil de usuario y el botón sign-out.
    this.userName.removeAttribute('hidden');
    this.userPic.removeAttribute('hidden');
    this.signOutButton.removeAttribute('hidden');

    // Ocultar el botón sign-in.
    this.signInButton.setAttribute('hidden', 'true');

    // Se cargan los mensajes existentes del chat.
    this.loadMessages();

    // Se guardan los token de Firebase Messaging Device y habilita notificaciones.
    //this.saveMessagingDeviceToken();
  } else { // El usuario ha salido
    // oculta los datos del perfil y el botón sign-out.
    this.userName.setAttribute('hidden', 'true');
    this.userPic.setAttribute('hidden', 'true');
    this.signOutButton.setAttribute('hidden', 'true');

    // Muestra el botón sign-in.
    this.signInButton.removeAttribute('hidden');
  }
};

// Regresa true si el usuario ha accedido. En cualquier otro caso se muestra false y se despliega un mensaje.
FriendlyChat.prototype.checkSignedInWithMessage = function() {
  /* TODO(DEVELOPER): Verifica si el usuario ha accedido a Firebase. */

  // Displiega un mensaje al usuario mediante un Toast.
  var data = {
    message: 'Primero debes acceder',
    timeout: 2000
  };
  this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
  return false;
};

// Guarda el messaging device token.
FriendlyChat.prototype.saveMessagingDeviceToken = function() {
  // TODO(DEVELOPER): Guarda el device token
};

// Solicita permisos para mostrar notificaciones.
FriendlyChat.prototype.requestNotificationsPermissions = function() {
  // TODO(DEVELOPER): Solicita permisos para enviar notificaciones.
};

// Le asigna una cadena vacía al MaterialTextField dado.
FriendlyChat.resetMaterialTextfield = function(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
};

// Template para los mensajes.
FriendlyChat.MESSAGE_TEMPLATE =
    '<div class="message-container">' +
      '<div class="spacing"><div class="pic"></div></div>' +
      '<div class="message"></div>' +
      '<div class="name"></div>' +
    '</div>';

// URL de la imagen que representa 'Loading'.
FriendlyChat.LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif';

// Despliega un mensaje en la UI.
FriendlyChat.prototype.displayMessage = function(key, name, text, picUrl, imageUri) {
  var div = document.getElementById(key);
  // Si un elemento para ese mensaje no existe todavía entonces se crea.
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = FriendlyChat.MESSAGE_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', key);
    this.messageList.appendChild(div);
  }
  if (picUrl) {
    div.querySelector('.pic').style.backgroundImage = 'url(' + picUrl + ')';
  }
  div.querySelector('.name').textContent = name;
  var messageElement = div.querySelector('.message');
  if (text) { // Si el mensaje es texto
    messageElement.textContent = text;
    // Remplaza todos los saltos de línea por <br>.
    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  } else if (imageUri) { // Si el mensaje tiene una imagen.
    var image = document.createElement('img');
    image.addEventListener('load', function() {
      this.messageList.scrollTop = this.messageList.scrollHeight;
    }.bind(this));
    this.setImageUrl(imageUri, image);
    messageElement.innerHTML = '';
    messageElement.appendChild(image);
  }
  // Muestra la card fading-in.
  setTimeout(function() {div.classList.add('visible')}, 1);
  this.messageList.scrollTop = this.messageList.scrollHeight;
  this.messageInput.focus();
};

// Habilita o desabilita el botón submit de los valores en los campos de entrada
FriendlyChat.prototype.toggleButton = function() {
  if (this.messageInput.value) {
    this.submitButton.removeAttribute('disabled');
  } else {
    this.submitButton.setAttribute('disabled', 'true');
  }
};

// Verifica que el Firebase SDK ha sido correctamente iniciado y configurado.
FriendlyChat.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('No has configurado o importado el Firebase SDK. ');
  }
};

window.onload = function() {
  window.friendlyChat = new FriendlyChat();
};
